#pragma once

#include <cstdint>
#include <string_view>

/*!
 * \brief Random number generator working at compile-time.
 * \see Method from https://youtu.be/rpn_5Mrrxf8
 */
template<typename RandomType = uint32_t>
class RandomNumberGenerator
{
public:
    static constexpr RandomType random(uint32_t iterations, std::string_view seed = __TIME__);

private:
    constexpr explicit RandomNumberGenerator(std::string_view seed);

    constexpr RandomType compute();

    static constexpr uint64_t incFromSeed(std::string_view seed);

    const std::string_view m_seed;
    uint64_t m_state = 0;
    uint64_t m_inc = incFromSeed(m_seed);
};

#include "rng/RandomNumberGenerator.tpp"
