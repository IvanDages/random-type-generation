template<typename RandomType>
class RandomNumberGenerator;

// Define the class so that the IDE does not raise false positive errors
template<typename RandomType>
class RandomNumberGenerator;

template<typename RandomType>
constexpr RandomNumberGenerator<RandomType>::RandomNumberGenerator(std::string_view seed)
    : m_seed{seed}
{
    // Initialize state
    compute();
}

template<typename RandomType>
constexpr RandomType RandomNumberGenerator<RandomType>::compute()
{
    auto oldState = m_state;
    // Advance internal state
    m_state = oldState * 6364136223846793005ULL + (m_inc | 1);
    // Calculate output function (XSH RR), uses old state for max ILP
    uint32_t xorshifted = ((oldState >> 18u) ^ oldState) >> 27u;
    uint32_t rot = oldState >> 59u;
    return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
}

template<typename RandomType>
constexpr uint64_t RandomNumberGenerator<RandomType>::incFromSeed(std::string_view seed)
{
    uint64_t shifted{0};

    for(int i = seed.size() ; i >= 0 ; i--)
    {
        shifted <<= 8u;
        shifted |= seed[i];
    }

    return shifted;
}

template<typename RandomType>
constexpr RandomType RandomNumberGenerator<RandomType>::random(uint32_t iterations, std::string_view seed)
{
    RandomNumberGenerator<RandomType> gen(seed);
    for (uint32_t i = 0; i < iterations; i++)
    {
        gen.compute();
    }
    return gen.compute();
}
