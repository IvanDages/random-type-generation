#pragma once

#include <cstdint>


constexpr uint32_t FLOAT_TYPE_ID = 0;
constexpr uint32_t DOUBLE_TYPE_ID = 1;


template<uint32_t TYPE_ID>
struct TypeIdTraits
{
};


template<>
struct TypeIdTraits<FLOAT_TYPE_ID>
{
    using Type = float;
};

template<>
struct TypeIdTraits<DOUBLE_TYPE_ID>
{
    using Type = double;
};
