#pragma once

#include <string_view>

template<typename TraitsType>
struct LogTraits
{
};

template<>
struct LogTraits<float>
{
    static constexpr inline std::string_view toString()
    {
        return "float";
    }
};

template<>
struct LogTraits<double>
{
    static constexpr inline std::string_view toString()
    {
        return "double";
    }
};
