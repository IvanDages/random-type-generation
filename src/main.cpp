#include <iostream>
#include <vector>

#include "traits/LogTraits.hpp"
#include "traits/TypeIdTraits.hpp"
#include "rng/RandomNumberGenerator.hpp"


template<uint32_t ITERATIONS>
using RandomFloatingPointType = typename TypeIdTraits<RandomNumberGenerator<>::random(ITERATIONS) % 2>::Type;

int main()
{
    RandomFloatingPointType<1> a;
    std::cout << "Random number type:             " << LogTraits<decltype(a)>::toString() << std::endl;
    RandomFloatingPointType<1> b;
    std::cout << "Same random number type:        " << LogTraits<decltype(b)>::toString() << std::endl;
    RandomFloatingPointType<2> c;
    std::cout << "Other random number type:       " << LogTraits<decltype(c)>::toString() << std::endl;
    RandomFloatingPointType<3> d;
    std::cout << "Yet another random number type: " << LogTraits<decltype(d)>::toString() << std::endl;

    return EXIT_SUCCESS;
}
