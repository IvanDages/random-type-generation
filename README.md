# Random Type Generation

Fun project to randomize the floating point precision of numbers at compile-time.

## Usage

The type `RandomFloatingPointType` is either `float` or `double`, depending on its parameter and the build time. The type of the following variables may change at every compilation: 

```cpp
// a and b always have the same type
RandomFloatingPointType<1> a;
RandomFloatingPointType<1> b;
// c and d may have the same type, or not
RandomFloatingPointType<2> c;
RandomFloatingPointType<3> d;
```
